TMPDIR=/tmp
test -e /dev/shm && TMPDIR=/dev/shm
LASTNFC=$TMPDIR/.lastnfc
LASTNFCTIME=$TMPDIR/.lastnfctime

echo 0 > $LASTNFCTIME
echo "SAVING LASTID TO $LASTNFC"
echo xxxxxxxxx > $LASTNFC
MODE=fireandforget;
while (true );do 
sleep 0.7; 
readerstate="$(timeout 2 nfc-poll 2>&1 )";


#echo "$readerstate"|grep "RDR_to_PC_DataBlock" && echo READ FAILED; 
## if we have a card , first prepare the comaprison string
echo "$readerstate"|grep -q "UID (NFCID1)" &&  { 
            dataline=$(echo "$readerstate"|grep "UID (NFCID1)") ;
            NFCID=${dataline/*:/};
            NFCID=${NFCID// /};
            echo -n 
## step 2 is to see if the card has changed so it will not retrigger
grep -q "^${NFCID}$" $LASTNFC  ||{ 
            which notify-send 2>&1 |grep notify-send -q && notify-send  "GOT NFC " # "$dataline" ;
            [[ "$MODE" = fireandforget ]] && { 
                  which notify-send 2>&1 |grep notify-send -q && notify-send "Virtual Keyboard is typing ... "; 
                  which cliclick 2>&1 |grep cliclick -q && {
                      cliclick "text:$NFCID";
                      cliclick "kp:return"
                      echo -n ; } ;
                  which cliclick 2>&1 |grep cliclick -q || {
    #                 which notify-send 2>&1 |grep notify-send -q && notify-send $NFCID
                      xdotool key $(echo $NFCID|sed 's/\(.\)/ \1/g') Return
    #                 xdotool key Return
                  echo -n ; }
                  echo ${NFCID} > $LASTNFC
                  date -u +%s > $LASTNFCTIME
            echo -n ; }
echo -n; }
## finally we purge the comparator every 20 seconds in case of re-triggering a card uid

[[ $(date -u +%s) -gt $(($(cat $LASTNFCTIME) + 20 )) ]] && { echo 0 > $LASTNFCTIME ; } ;

#echo $NFCID $(cat $LASTNFC)
### check for found uid and changed card since we ignore the "remove card" hint from subprogram nfc-poll
##end case of found card
echo -n ; } ;done
